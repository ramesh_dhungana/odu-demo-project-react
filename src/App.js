import React, { useState, useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Login from "./components/Login";
import Register from "./components/Register";
import Home from "./components/Home";
import Dashboard from "./components/Dashboard";
import EventBus from "./common/EventBus";
import { PrivateRoute } from "./PrivateRoute";
import { logout } from "./actions/auth";

const App = () => {
	const [showAdminBoard, setShowAdminBoard] = useState(false);
	const [showSuperVisorBoard, setShowSuperVisorBoard] = useState(false);
	const [showAgentBoard, setShowAgentBoard] = useState(false);
	const { user: currentUser } = useSelector((state) => state.auth);
	const dispatch = useDispatch();

	const logOut = useCallback(() => {
		dispatch(logout());
	}, [dispatch]);

	useEffect(() => {
		if (currentUser) {
			setShowAdminBoard(currentUser.roles.includes("ADMIN"));
			setShowSuperVisorBoard(currentUser.roles.includes("SUPERVISOR"));
			setShowAgentBoard(currentUser.roles.includes("AGENT"));
		} else {
			setShowSuperVisorBoard(false);
			setShowAdminBoard(false);
			setShowAgentBoard(false);
		}

		EventBus.on("logout", () => {
			logOut();
		});

		return () => {
			EventBus.remove("logout");
		};
	}, [currentUser, logOut]);

	return (
		<Router>
			<div>
				<nav className="navbar navbar-expand navbar-dark bg-primary">
					{currentUser ? (
						<div>
							{showAdminBoard && (
								<Link to={"/dashboard"} className="navbar-brand">
									{currentUser.roles[0]} DASHBOARD
								</Link>
							)}

							{showSuperVisorBoard && (
								<Link to={"/dashboard"} className="navbar-brand">
									{currentUser.roles[0]} DASHBOARD
								</Link>
							)}

							{showAgentBoard && (
								<Link className="navbar-brand" to={"/dashboard"}>
									{currentUser.roles[0]} DASHBOARD
								</Link>
							)}
						</div>
					) : (
						<Link to={"/"} className="navbar-brand">
							Home
						</Link>
					)}

					{currentUser ? (
						<div className="navbar-nav ml-auto">
							<button
								className="navbar-toggler"
								type="button"
								data-toggle="collapse"
								data-target="#navbar-list-4"
								aria-controls="navbarNav"
								aria-expanded="false"
								aria-label="Toggle navigation"
							>
								<span className="navbar-toggler-icon"></span>
							</button>
							<div className="collapse navbar-collapse" id="navbar-list-4">
								<ul className="navbar-nav">
									<li className="nav-item dropdown">
										<a
											className="nav-link dropdown-toggle"
											href="#"
											id="navbarDropdownMenuLink"
											role="button"
											data-toggle="dropdown"
											aria-haspopup="true"
											aria-expanded="false"
										>
											<img
												src={currentUser.profilePicture}
												width="40"
												height="40"
												className="rounded-circle"
											/>
										</a>
										<div
											className="dropdown-menu"
											aria-labelledby="navbarDropdownMenuLink"
										>
											<div className="dropdown-item">
												Name: {currentUser.displayName}
											</div>
											<div className="dropdown-item">
												Role: {currentUser.roles[0]}
											</div>
											<a className="dropdown-item" onClick={logOut}>
												Log Out
											</a>
										</div>
									</li>
								</ul>
							</div>
							<li className="nav-item">
								<a href="/login" className="nav-link" onClick={logOut}>
									LogOut
								</a>
							</li>
						</div>
					) : (
						<div className="navbar-nav ml-auto">
							<li className="nav-item">
								<Link to={"/login"} className="nav-link">
									Login
								</Link>
							</li>
						</div>
					)}
				</nav>

				<div className="container mt-3">
					<Switch>
						<Route exact path={["/", "/home"]} component={Home} />
						<Route exact path="/login" component={Login} />
						<Route exact path="/dashboard" component={Dashboard} />
						{/* <Route exact path="/profile" component={Profile} /> */}
						{/* <Route
							path="/dashboard"
							element={<PrivateRoute component={Dashboard} />}
						/> */}
					</Switch>
				</div>
			</div>
		</Router>
	);
};

export default App;
