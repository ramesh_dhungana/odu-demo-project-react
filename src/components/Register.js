import React, { useState, useEffect } from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import InputLabel from "@mui/material/InputLabel";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { register } from "../actions/auth";
import { useDispatch, useSelector } from "react-redux";
import UserService from "../services/user.service";

const theme = createTheme();

const Register = (props) => {
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [role, setRole] = useState("");
	const [supervisor, setSupervisor] = useState("");
	const [successful, setSuccessful] = useState(false);
	const [allSupervisors, setAllSupervisors] = useState([]);
	const dispatch = useDispatch();

	useEffect(() => {
		UserService.getSupervisors().then(
			(response) => {
				const allSupervisors = response.data.data;
				setAllSupervisors(allSupervisors);
			},
			(error) => {
				const _content =
					(error.response &&
						error.response.data &&
						error.response.data.message) ||
					error.message ||
					error.toString();
				setAllSupervisors(_content);
			}
		);
	}, []);

	const onChangeFirstName = (e) => {
		const firstName = e.target.value;
		setFirstName(firstName);
	};

	const onChangeLastName = (e) => {
		const lastName = e.target.value;
		setLastName(lastName);
	};

	const onChangeEmail = (e) => {
		const email = e.target.value;
		setEmail(email);
	};

	const onChangePassword = (e) => {
		const password = e.target.value;
		setPassword(password);
	};

	const onChangeRole = (e) => {
		const role = e.target.value;
		setRole(role);
	};

	const onChangeSupervisor = (e) => {
		const supervisor = e.target.value;
		setSupervisor(supervisor);
	};

	const handleRegister = (event) => {
		event.preventDefault();
		dispatch(register(firstName, lastName, email, password, role, supervisor))
			.then((_) => {
				window.location.reload(false);
			})
			.catch(() => {
			});
	};

	return (
		<ThemeProvider theme={theme} >
			<Container component="main" maxWidth="xs">
				<CssBaseline />
				<Box
					sx={{
						marginTop: 8,
						display: "flex",
						flexDirection: "column",
						alignItems: "center",
					}}
				>
					<Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
						<LockOutlinedIcon />
					</Avatar>
					<Typography component="h1" variant="h5">
						Register an User
					</Typography>
					<Box component="form" noValidate sx={{ mt: 3 }}>
						<Grid container spacing={2}>
							<Grid item xs={12} sm={6}>
								<TextField
									autoComplete="given-name"
									name="firstName"
									required
									fullWidth
									id="firstName"
									label="First Name"
									autoFocus
									onChange={onChangeFirstName}
								/>
							</Grid>
							<Grid item xs={12} sm={6}>
								<TextField
									required
									fullWidth
									id="lastName"
									label="Last Name"
									name="lastName"
									autoComplete="family-name"
									onChange={onChangeLastName}
								/>
							</Grid>
							<Grid item xs={12}>
								<TextField
									required
									fullWidth
									id="email"
									label="Email Address"
									name="email"
									autoComplete="email"
									onChange={onChangeEmail}
								/>
							</Grid>
							<Grid item xs={12}>
								<InputLabel id="role">Select a Role</InputLabel>
								<Select
									style={{ minWidth: 420 }}
									labelId="Roles"
									name="role"
									id="role"
									value={role}
									label="Roles"
									onChange={onChangeRole}
									size="large"
								>
									<MenuItem disabled value="">
										Select a Role
									</MenuItem>
									<MenuItem value="SUPERVISOR">SUPERVISOR</MenuItem>
									<MenuItem value="AGENT">AGENT</MenuItem>
									<MenuItem value="ADMIN">ADMIN</MenuItem>
								</Select>
							</Grid>
							{
								role === 'AGENT' ?
									<Grid item xs={12}>
										<InputLabel id="role">Select a Supervisor</InputLabel>
										<Select
											style={{ minWidth: 420 }}
											labelId="supervisor"
											name="supervisor"
											id="supervisor"
											value={supervisor}
											label="supervisor"
											onChange={onChangeSupervisor}
											size="large"
										>
											<MenuItem disabled value="">
												Select a supervisor
											</MenuItem>
											{allSupervisors.map(item => {
												return <MenuItem key={item.id} value={item.id}>{item['firstName']} {item['lastName']}</MenuItem>
											})}
										</Select>
									</Grid>
									: null
							}

							<Grid item xs={12}>
								<TextField
									required
									fullWidth
									name="password"
									label="Password"
									type="password"
									id="password"
									onChange={onChangePassword}
									autoComplete="password"
								/>
							</Grid>
						</Grid>
						<Button
							type="submit"
							onClick={handleRegister}
							fullWidth
							variant="contained"
							sx={{ mt: 3, mb: 2 }}
							disabled={
								!email ||
								!role ||
								!password ||
								!firstName ||
								!lastName
							}
						>
							Register
						</Button>
					</Box>
				</Box>
			</Container>
		</ThemeProvider >
	);
}

export default Register;
