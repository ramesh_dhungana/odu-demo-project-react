import React, { useEffect } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { useSelector } from "react-redux";
import DashboardAdmin from "./DashboardAdmin";
import DashboardAgent from "./DashboardAgent";
import DashboardSupervisor from "./DashboardSupervisor";
// import { useRouter } from "../hooks/useRouter";

const Dashboard = () => {
	const userInfo = useSelector((state) => state.auth);
	const selectedView = () => {
		if (userInfo?.user?.roles?.includes("AGENT")) {
			return <DashboardAgent />;
		} else if (userInfo.user?.roles?.includes("ADMIN")) {
			return <DashboardAdmin />;
		} else if (userInfo?.user?.roles?.includes("SUPERVISOR")) {
			return <DashboardSupervisor />;
		}
	};
	return userInfo && selectedView();
};

export default Dashboard;
