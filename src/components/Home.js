import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { useRouter } from "./../hooks/useRouter";

const Home = () => {
  const { user: currentUser } = useSelector((state) => state.auth);
  const { push } = useRouter();

  useEffect(() => {
    if (currentUser) {
      push("/dashboard");
    }
  }, []);

  return (
    <div className="container">
      <header className="jumbotron">
        <h3>Welcome to ODU!</h3>
      </header>
    </div>
  );
};

export default Home;
