import React, { useState, useEffect } from "react";

import UserService from "../services/user.service";
import EventBus from "../common/EventBus";

import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";
import MessageIcon from '@mui/icons-material/Message';
import Tooltip from '@mui/material/Tooltip';
import VisibilityIcon from '@mui/icons-material/Visibility';

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { Chat } from '@progress/kendo-react-conversational-ui';
import { useSelector } from "react-redux";


function createData(agent) {
	const { firstName, lastName, email } = agent;
	return { firstName, lastName, email };
}

let rows = [];
function setRows(agents = []) {
	agents.forEach((ag) => {
		rows = [...rows, createData(ag)]
	})
}
let currentUserData = {};

function setCurrentUserData(user) {
	currentUserData = {
		id: user.id,
		name: `${user.firstName} ${user.lastName}`,
		avatarUrl: user?.profilePicture ? user?.profilePicture : "https://via.placeholder.com/24/008000/008000.png"
	}
};

const MessageTemplate = props => {
	return <div className="k-bubble">
		<div>{props.item.text}</div>
	</div>;
};

const DashboardSupervisor = () => {
	const { user } = useSelector((state) => state.auth);
	const [agents, setAgents] = useState([]);
	const [selectedAgent, setSelectedAgent] = React.useState(null);
	const [open, setOpen] = React.useState(false);
	const [messages, setMessages] = React.useState([]);

	const addNewMessage = event => {
		addMessage(event.message);
	};

	const getMessages = (from, to) => {
		UserService.getMessages(from, to).then(
			(response) => {
				const messages = response.data.data;
				messages.forEach(msg => {
					msg.timestamp = new Date(msg.timestamp)
				})
				setMessages(messages);
			},
			(error) => {
				const _content =
					(error.response &&
						error.response.data &&
						error.response.data.message) ||
					error.message ||
					error.toString();
				setMessages([]);
			}
		);
	}

	const addMessage = (eventMessage) => {
		const author = `${user.firstName} ${user.lastName}`;
		const text = eventMessage.text;
		const to = selectedAgent.id;
		UserService.addMessage(author, text, to).then(
			(_) => {
				setMessages([...messages, eventMessage]);
			},
			(error) => {
				const _content =
					(error.response &&
						error.response.data &&
						error.response.data.message) ||
					error.message ||
					error.toString();
				setMessages([]);
			}
		);
	}

	const handleClickOpen = (selectedAgent) => {
		setOpen(true);
		setSelectedAgent(selectedAgent);
		getMessages(currentUserData.id, selectedAgent.id);
	};

	const handleClose = () => {
		setOpen(false);
	};

	useEffect(() => {
		UserService.getAgents().then(
			(response) => {
				const agents = response.data.data;
				setAgents(agents);
				setRows(agents);
				setCurrentUserData(user);
			},
			(error) => {
				const _content =
					(error.response &&
						error.response.data &&
						error.response.data.message) ||
					error.message ||
					error.toString();
				setAgents(_content);
				// if (error.response) {
				// 	EventBus.dispatch("logout");
				// }
			}
		);
	}, []);

	return (
		<>
			<div>
				<p>Welcome to ODU <b>{user.firstName}!</b></p>
				<br></br>
				<h3>Your Agents</h3>
				<br />
				<TableContainer component={Paper}>
					<Table sx={{ minWidth: 650 }} aria-label="simple table">
						<TableHead>
							<TableRow>
								<TableCell align="left">S.N</TableCell>
								<TableCell align="left">First Name</TableCell>
								<TableCell align="left">Last Name</TableCell>
								<TableCell align="left">email</TableCell>
								<TableCell align="left">Role</TableCell>
								<TableCell align="right">Action</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{agents.map((row, index) => (
								<TableRow
									key={index}
									sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
								>
									<TableCell align="left">
										{index + 1}
									</TableCell>
									<TableCell align="left">
										{row.firstName}
									</TableCell>
									<TableCell align="left">{row.lastName}</TableCell>
									<TableCell align="left">{row.email}</TableCell>
									<TableCell align="left">{row.roles[0]}</TableCell>
									<TableCell
										align="right"
										onClick={() => handleClickOpen(row)}
									>
										<span style={{ cursor: "pointer", marginRight: "10px" }}>
											<Tooltip title="send message">
												<MessageIcon />
											</Tooltip>
										</span>
									</TableCell>
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
			</div>
			<div>
				<Dialog open={open} onClose={handleClose}>
					<DialogTitle>Send Message to <b>{selectedAgent?.firstName} {selectedAgent?.lastName}</b></DialogTitle>
					<DialogContent>
						<Chat user={currentUserData} messages={messages} onMessageSend={addNewMessage} width={1000} messageTemplate={MessageTemplate} />
					</DialogContent>
					<DialogActions>
						<Button onClick={handleClose}>Close</Button>
					</DialogActions>
				</Dialog>
			</div>
		</>
	);
};

export default DashboardSupervisor;