import React from "react";
import Register from "./Register";
import { useSelector } from "react-redux";


const DashboardAdmin = () => {
	const { user } = useSelector((state) => state.auth);
	return (
		<div>
			<p>Welcome to ODU <b>{user.firstName}!</b></p>
			<Register />
		</div>
	);
};

export default DashboardAdmin;
