import * as firebase from 'firebase';

let config = {
	apiKey: "AIzaSyDZ8fduTDerDtlDUknYzo2j2hObokJxe30",
	authDomain: "odu-first-demo-project.firebaseapp.com",
	projectId: "odu-first-demo-project",
	storageBucket: "odu-first-demo-project.appspot.com",
	messagingSenderId: "709490978390",
	appId: "1:709490978390:web:89c351981488340b2155f7",
	measurementId: "G-WL7E2M9Y7S"
};
firebase.initializeApp(config);

export default firebase;