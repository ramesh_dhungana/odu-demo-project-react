import axios from "axios";
import authHeader from "./auth-header.service";

const API_URL =
	"http://localhost:8080/api/v1/";

const getUser = (userId) => {
	return axios.get(API_URL + "user", { headers: authHeader(), params: { userId } });
};

const getMySupervisor = (userId) => {
	return getUser(userId);
};

const getUsers = (role) => {
	return axios.get(API_URL + "users", {
		headers: authHeader(),
		params: { role },
	});
};

const getAgents = () => {
	return getUsers("AGENT");
};

const getSupervisors = () => {
	return getUsers("SUPERVISOR");
};

const addMessage = (author, text, to) => {
	return axios.post(API_URL + "add-message", { author, text, to }, {
		headers: authHeader(),
	});
};

const getMessages = (from, to) => {
	return axios.get(API_URL + "messages", {
		headers: authHeader(),
		params: { to, from },
	});
};

const userService = {
	getUser,
	getAgents,
	getMessages,
	addMessage,
	getSupervisors,
	getMySupervisor
};

export default userService;
