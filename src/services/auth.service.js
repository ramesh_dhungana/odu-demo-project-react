import axios from "axios";

const API_URL = "http://localhost:8080/api/v1/";

const register = (firstName, lastName, email, password, role, supervisor) => {
	return axios.post(API_URL + "auth/register", {
		firstName, lastName, email, password, role, supervisor
	});
};

const login = async (email, password) => {
	const response = await axios.post(API_URL + "auth/login", {
		email,
		password,
	});
	if (response.data) {
		localStorage.setItem("user", JSON.stringify(response.data.data));
	}
	return response.data.data;
};

const logout = () => {
	localStorage.removeItem("user");
};

export default {
	register,
	login,
	logout,
};